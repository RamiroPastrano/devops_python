#### IMPORTACIÓN DE LIBRERÍAS DE GESTIÓN DE BASE DE DATOS
import mysql.connector


##### CONEXIÓ A MYSQL
def connect_MySQL_Custom_Connection(host, user, password, db, port):
  connection = mysql.connector.connect(
      host=host,
      user=user,
      password=password,
      database=db,
      port=port
  )
  return connection