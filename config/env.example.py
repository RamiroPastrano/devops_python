#### ARCHIVO DE EJEMPLO PARA VARIABLES DE ENTORNO
#### DE CONEXIÓN A MYSQL
#### SE REQUIERE QUE SUSTITUYAS LOS VALORES NECESARIOS PARA TU CONEXIÓN

###  SE REQUIERE IMPORTAR EL MÓDULO PARA CREAR UN DICT
from pydantic_settings import BaseSettings


###  SE DEFINE LA CLASE PRINCIPAL CON LOS DATOS DE CONEXIÓN --  ESTO CREA UN DICT 
class Settings_MySQL(BaseSettings):
    hostname:   str = "localhost"     #Generalmente es posible realizar la conexión a localhost si tu proyecto vive en el mismo lugar que tu api.
    user:       str = "youruser"
    password:   str = "yourpassword"
    db:         str = "yourdb"
    port:       str = "3306"          #El puerto default es 3306 pero si por alguna razón lo cambiaste también es necesario cambiarlo aquí.