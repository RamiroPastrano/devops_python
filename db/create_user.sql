/* Primero creamos el usuario y la base de datos a la que tendrá acceso*/
CREATE USER 'nombre_de_usuario'@'localhost' IDENTIFIED BY 'tu_contraseña';
/* Posteriormente asignamos los permisos a ese usuario para insertar datos */
GRANT INSERT ON nombre_de_la_base_de_datos.nginx_logs TO 'nombre_de_usuario'@'localhost';