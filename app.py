from typing import Union
import json
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.utils import get_openapi

### API PRIVADA ###
import routes.root as rootRouter
import routes.logs as rootLogs





description = "API PYTHON FOR LOGS - TEST"

tags_metadata = [
    {
        "name": "Inicio",
        "description": "Método de entrada, raiz de acceso a API, cualquier response fuera de 200 significa que la api no ha sido montada correctamente o bien el server no está siendo redireccionado correctamente"
    },
    {
        "name": "Logs",
        "description": "Método de entrada a los servicios de Logs, para leer los logs de las instancias activas"
    }
]


app = FastAPI(
    title="API - PYTHON FOR DEVOPS",
    description=description,
    version="0.0.1",
    terms_of_service="https://ramiropastrano.com",
    
    contact={
        
        "name": "RAMIRO PASTRANO",
        "url": "https://ramiropastrano.com",
        "email": "ramiropastrano@hotmail.com"

    },
    license_info={
        "name": "ConnectAPI License",
        "url": "https://ramiropastrano.com/"
    },
    openapi_tags=tags_metadata
)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"]
)

enviroment = {}



@app.on_event('startup')
async def iniciarEnviroment():
    print("Iniciando Enviroment")
   
    print("===Database Started")
    ### API  PRIVADA ###        
    
    app.include_router(rootRouter.router)
    app.include_router(rootLogs.router)
        
    
    
