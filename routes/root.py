from fastapi.responses import JSONResponse
from fastapi import APIRouter, Request


from modules.inicio import *;

router = APIRouter(
    tags=["Inicio"]
)

@router.get("/",
    responses={
        
        200: {
            "description": "Operación exitosa. Se ha inicializado la api correctamente",
            "content":{
                "application/json": {
                    "example": {
                        "codigo": 200,
                        "mensaje": "Operación Exitosa",
                        "data": {
                            "API": "API HISTORIA CLÍNICA",
                            "Status": "Success",
                            "Description": "Fast API funcionando correctamente"
                        }
                    }
                }
            }
            
            
        }
    }
)
def inicio(request: Request):
    ip  = str(request.client.host)
    url = str(request.url.path)
    return route_inicio(ip, url)
