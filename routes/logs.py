from fastapi import APIRouter
import asyncio

###Custom Importers
import modules.logs_nginx as LOGS_NGINX

'''Ruta principal para las rutas de carga de archivos CSV para importar datos de salesforce en la base
   de datos de MySQL
'''
router = APIRouter(
    prefix=("/logs"),
    tags=["Logs"]
)
@router.get("/", responses={
    400:{
        "description": "Operación exitosa.",
        "content":{
            "application/json": {
                "example": {
                    "codigo": 400,
                    "mensaje": "Operación Errónea",
                    "data": {
                        "Status": "Failed",
                        "Description": "Lo sentimos no existe una asignación a este servicio."
                    }
                }
            }
        }
    }
})
def inicio():
    
    response = {
        "codigo": 400,
        "mensaje": "Operación Errónea",
        "data": {
            "Status": "Failed",
            "Description": "Lo sentimos no existe una asignación a este servicio."
        }
    }
    return response


@router.get("/nginx", responses={
        200: {
            "description": "Operación exitosa.",
            "content":{
                "application/json": {
                    "example": {
                        "codigo": 200,
                        "mensaje": "Operación Exitosa",
                        "data": {
                            "Status": "Success",
                            "Description": "Datos cargados exitosamente",
                        }
                    }
                }
            }
            
        },
        400: {
            "description": "Operación exitosa.",
            "content":{
                "application/json": {
                    "example": {
                        "codigo": 400,
                        "mensaje": "Operación Errónea",
                        "data": {
                            "Status": "Failed",
                            "Description": "Ocurrió un error al procesar el arhivo - valide los datos y el tipo de archivo (CSV)"
                        }
                    }
                }
            }
        }
    }
)
# Ruta para iniciar la lectura de los logs
async def Logs_Nginx():
    asyncio.create_task(LOGS_NGINX.read_and_process_logs())
    return {"message": "Lectura de logs iniciada."}
