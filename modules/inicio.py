import datetime
def route_inicio(ip, url):
    #return {"API": "API HISTORIA CLINICA EN PYTHON", "Status": "Success", "Description": "Fast API funcionando correctamente"}
    response = { 
        "codigo": 200,
        "mensaje": "Operación Exitosa",
        "data": {
            "API": "API HISTORIA CLÍNICA",
            "Status": "Success",
            "Description": "Fast API funcionando correctamente",
            "data": {
                "Ip": ip,
                "Url": url,
                "date_conection": datetime.datetime.now()
            }
            
        }
    }

    return response
