import asyncio
import re
from datetime import datetime, timezone, timedelta
#### variables de Entorno
import config.env as ENV
SETTINGS = ENV.Settings_MySQL()
###
from datetime import datetime


##custom importers
import config.db as DB

##



# Configuración de la conexión a MySQL
MYSQL_HOST      = SETTINGS.hostname
MYSQL_USER      = SETTINGS.user
MYSQL_PASSWORD  = SETTINGS.password
MYSQL_DB        = SETTINGS.db
MYSQL_PORT      = SETTINGS.port

# Configuración del archivo de logs de Nginx
NGINX_LOG_FILE = '/var/log/nginx/access.log'

# Expresión regular para extraer los datos del log de Nginx
LOG_STRUCTURE = r'^(\S+) (\S+) (\S+) \[(.*?)\] "(.*?)" (\d+) (\d+) "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)" "(.*?)"'

# Función para procesar una línea de log de Nginx y convertirla en un diccionario
def process_log_line(line):
    print(f"===Line {line}")
    match = re.match(LOG_STRUCTURE, line)
    print("===match")
    print(match)
    if match:
        data = match.groups()
        return {
            'remote_addr': data[0],
            'remote_user': data[1],
            'time_local': data[2],
            'request': data[3],
            'status': data[4],
            'body_bytes_sent': data[5],
            'http_referer': data[6],
            'http_user_agent': data[7],
            'http_x_forwarded_for': data[8],
            'server_name': data[9],
            'http_host': data[10],
            'http_connection': data[11],
            'http_cache_control': data[12],
            'http_upgrade_insecure_requests': data[13],
            'http_accept': data[14],
            'http_accept_encoding': data[15],
            'http_accept_language': data[16],
            'http_cookie': data[17],
            'http_x_requested_with': data[18],
            'http_dnt': data[19],
            'http_if_modified_since': data[20],
            'http_if_none_match': data[21],
            'http_content_type': data[22],
            'http_origin': data[23],
            'http_content_length': int(data[24]),
            'http_via': data[25],
            'http_x_forwarded_proto': data[26],
            'http_x_forwarded_ssl': data[27],
            'http_x_forwarded_port': data[28],
            'http_x_real_ip': data[29],
            'http_connection_upgrade': data[30],
            'http_referer_encoding': data[31]
        }
    else:
        print("===Sin coincidencia")
        return None
    
def process_log_line_parts(line):
    print(f"===Line {line}")
    parts = line.split(' ')
    if len(parts) >= 36:
        return {
            'remote_addr': parts[0],
            'remote_user': parts[1],
            'time_local': parse_date(parts[3]),#parts[3] + ' ' + parts[4].replace('[', '') + ' ' + parts[4].replace(']', ''),
            'request': parts[5] ,
            'status': parts[8],
            'body_bytes_sent': parts[9],
            'http_referer': parts[6] ,
            'http_user_agent': parts[7] + parts[10] + parts[11] + ' '.join(parts[12:]),
            'http_x_forwarded_for': parts[12] if len(parts) >= 13 else None,
            'server_name': parts[13] if len(parts) >= 14 else None,
            'http_host': parts[14] if len(parts) >= 15 else None,
            'http_connection': parts[15] if len(parts) >= 16 else None,
            'http_cache_control': parts[16] if len(parts) >= 17 else None,
            'http_upgrade_insecure_requests': parts[17] if len(parts) >= 18 else None,
            'http_accept': parts[18] if len(parts) >= 19 else None,
            'http_accept_encoding': parts[19] if len(parts) >= 20 else None,
            'http_accept_language': parts[20] if len(parts) >= 21 else None,
            'http_cookie': parts[21] if len(parts) >= 22 else None,
            'http_x_requested_with': parts[22] if len(parts) >= 23 else None,
            'http_dnt': parts[23] if len(parts) >= 24 else None,
            'http_if_modified_since': parts[24] if len(parts) >= 25 else None,
            'http_if_none_match': parts[25] if len(parts) >= 26 else None,
            'http_content_type': parts[26] if len(parts) >= 27 else None,
            'http_origin': parts[27] if len(parts) >= 28 else None,
            'http_content_length': parts[28] if len(parts) >= 29 else None,
            'http_via': parts[29] if len(parts) >= 30 else None,
            'http_x_forwarded_proto': parts[30] if len(parts) >= 31 else None,
            'http_x_forwarded_ssl': parts[31] if len(parts) >= 32 else None,
            'http_x_forwarded_port': parts[32] if len(parts) >= 33 else None,
            'http_x_real_ip': parts[33] if len(parts) >= 34 else None,
            'http_connection_upgrade': parts[34] if len(parts) >= 35 else None,
            'http_referer_encoding': parts[35] if len(parts) >= 36 else None
        }
    else:
        print("===Insufficient parts")
        return None

# Función para guardar los datos en MySQL
def save_to_mysql(data):
    print("===intentando imprimir")
    print(data)
    with DB.connect_MySQL_Custom_Connection(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB, MYSQL_PORT) as connection:
        cursor = connection.cursor()
        try:
            cursor.execute(f"""
                INSERT INTO nginx_logs (
                    remote_addr, remote_user, time_local, request, status, body_bytes_sent, http_referer, http_user_agent,
                    http_x_forwarded_for, server_name, http_host, http_connection, http_cache_control,
                    http_upgrade_insecure_requests, http_accept, http_accept_encoding, http_accept_language,
                    http_cookie, http_x_requested_with, http_dnt, http_if_modified_since, http_if_none_match,
                    http_content_type, http_origin, http_content_length, http_via, http_x_forwarded_proto,
                    http_x_forwarded_ssl, http_x_forwarded_port, http_x_real_ip, http_connection_upgrade,
                    http_referer_encoding
                ) VALUES (
                    %(remote_addr)s, %(remote_user)s, %(time_local)s, %(request)s, %(status)s, %(body_bytes_sent)s,
                    %(http_referer)s, %(http_user_agent)s, %(http_x_forwarded_for)s, %(server_name)s, %(http_host)s,
                    %(http_connection)s, %(http_cache_control)s, %(http_upgrade_insecure_requests)s, %(http_accept)s,
                    %(http_accept_encoding)s, %(http_accept_language)s, %(http_cookie)s, %(http_x_requested_with)s,
                    %(http_dnt)s, %(http_if_modified_since)s, %(http_if_none_match)s, %(http_content_type)s,
                    %(http_origin)s, %(http_content_length)s, %(http_via)s, %(http_x_forwarded_proto)s,
                    %(http_x_forwarded_ssl)s, %(http_x_forwarded_port)s, %(http_x_real_ip)s, %(http_connection_upgrade)s,
                    %(http_referer_encoding)s
                )
            """, data)
            # Confirma los cambios en la base de datos
            connection.commit()

        except Exception as e:
            print(f'===Error al insertar en MySQL: {e}')
            connection.rollback()
        finally:
            print("===Insertion complete")
            cursor.close()
            connection.close()

# Función para convertir cadenas de texto a enteros
def parse_int(int_str):
    if not int_str:
        return None
    if isinstance(int_str, str):
        # Filtrar caracteres no numéricos
        int_str_filtered = ''.join(filter(str.isdigit, int_str))
    else:
        int_str_filtered = int_str
    
    # Convertir la cadena filtrada a un entero
    try:
        return int(int_str_filtered)
    except ValueError:
        print("==== Ha fallado la conversión de Int " + int_str)
        return None

def parse_date(fecha_str):
    # Quitar el primer corchete y limpiar la fecha
    fecha_str = fecha_str.replace('[', '')

    # Definir el formato de fecha
    formato_fecha = '%d/%b/%Y:%H:%M:%S'

    try:
        # Convertir la fecha a un objeto datetime
        fecha_datetime = datetime.strptime(fecha_str, formato_fecha)
        return fecha_datetime
    except ValueError:
        return None

# Función para leer el archivo de logs de Nginx y procesar cada línea
async def read_and_process_logs():
    print("===iniciando proceso de lectura")
    with open(NGINX_LOG_FILE, 'r') as f:
        while True:
            line = f.readline()
            if line:
                data = process_log_line_parts(line)
                if data:
                    save_to_mysql(data)
            else:
                await asyncio.sleep(0.1)


