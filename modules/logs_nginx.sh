#!/bin/bash


# Variables de configuración
LOG_FILE="/var/log/nginx/access.log"
LAST_LINE_FILE="/path/to/last_line.txt"  # Ruta al archivo que guarda la última línea procesada
MYSQL_HOST="mysql.test"
MYSQL_DB="Logs"
MYSQL_USER="root"
MYSQL_PASS="@dm1n2024"


# Función para obtener la última línea procesada
get_last_line() {
   if [ -f "$LAST_LINE_FILE" ]; then
       LAST_LINE=$(cat "$LAST_LINE_FILE")
   else
       LAST_LINE=""
   fi
}


# Función para procesar los logs de Nginx y guardarlos en MySQL
process_logs() {
   get_last_line
   # Leer el archivo de logs de Nginx línea por línea desde la última línea procesada
   if [ -z "$LAST_LINE" ]; then
       sed '1!G;h;$!d' "$LOG_FILE" | while IFS= read -r line; do
           # Extraer los campos del log de Nginx
           time_local=$(echo "$line" | awk '{print $4, $5}')
           remote_addr=$(echo "$line" | awk '{print $1}')
           remote_user="-"  # No se registra en los logs de acceso de Nginx
           request=$(echo "$line" | awk '{print $6}')
           status=$(echo "$line" | awk '{print $9}')
           body_bytes_sent=$(echo "$line" | awk '{print $10}')
           http_referer=$(echo "$line" | awk '{print $11}')
           http_user_agent=$(echo "$line" | awk '{for (i=12; i<NF; i++) printf $i" "; print $NF}')
           http_x_forwarded_for=$(echo "$line" | awk '{print $12}')
           server_name=$(echo "$line" | awk '{print $13}')
           http_host=$(echo "$line" | awk '{print $14}')
           http_connection=$(echo "$line" | awk '{print $15}')
           http_cache_control=$(echo "$line" | awk '{print $16}')
           http_upgrade_insecure_requests=$(echo "$line" | awk '{print $17}')
           http_accept=$(echo "$line" | awk '{print $18}')
           http_accept_encoding=$(echo "$line" | awk '{print $19}')
           http_accept_language=$(echo "$line" | awk '{print $20}')
           http_cookie=$(echo "$line" | awk '{print $21}')
           http_x_requested_with=$(echo "$line" | awk '{print $22}')
           http_dnt=$(echo "$line" | awk '{print $23}')
           http_if_modified_since=$(echo "$line" | awk '{print $24}')
           http_if_none_match=$(echo "$line" | awk '{print $25}')
           http_content_type=$(echo "$line" | awk '{print $26}')
           http_origin=$(echo "$line" | awk '{print $27}')
           http_content_length=$(echo "$line" | awk '{print $28}')
           http_via=$(echo "$line" | awk '{print $29}')
           http_x_forwarded_proto=$(echo "$line" | awk '{print $30}')
           http_x_forwarded_ssl=$(echo "$line" | awk '{print $31}')
           http_x_forwarded_port=$(echo "$line" | awk '{print $32}')
           http_x_real_ip=$(echo "$line" | awk '{print $33}')
           http_connection_upgrade=$(echo "$line" | awk '{print $34}')
           http_referer_encoding=$(echo "$line" | awk '{print $35}')


           # Insertar los campos en la base de datos MySQL
           mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASS" "$MYSQL_DB" <<EOF
           INSERT INTO nginx_logs (time_local, remote_addr, remote_user, request, status, body_bytes_sent, http_referer, http_user_agent, http_x_forwarded_for, server_name, http_host, http_connection, http_cache_control, http_upgrade_insecure_requests, http_accept, http_accept_encoding, http_accept_language, http_cookie, http_x_requested_with, http_dnt, http_if_modified_since, http_if_none_match, http_content_type, http_origin, http_content_length, http_via, http_x_forwarded_proto, http_x_forwarded_ssl, http_x_forwarded_port, http_x_real_ip, http_connection_upgrade, http_referer_encoding)
           VALUES ('$time_local', '$remote_addr', '$remote_user', '$request', '$status', '$body_bytes_sent', '$http_referer', '$http_user_agent', '$http_x_forwarded_for', '$server_name', '$http_host', '$http_connection', '$http_cache_control', '$http_upgrade_insecure_requests', '$http_accept', '$http_accept_encoding', '$http_accept_language', '$http_cookie', '$http_x_requested_with', '$http_dnt', '$http_if_modified_since', '$http_if_none_match', '$http_content_type', '$http_origin', '$http_content_length', '$http_via', '$http_x_forwarded_proto', '$http_x_forwarded_ssl', '$http_x_forwarded_port', '$http_x_real_ip', '$http_connection_upgrade', '$http_referer_encoding');
EOF
       done
   else
       sed "1,/$LAST_LINE/d" "$LOG_FILE" | while IFS= read -r line; do
           # Extraer los campos del log de Nginx
           time_local=$(echo "$line" | awk '{print $4, $5}')
           remote_addr=$(echo "$line" | awk '{print $1}')
           remote_user="-"  # No se registra en los logs de acceso de Nginx
           request=$(echo "$line" | awk '{print $6}')
           status=$(echo "$line" | awk '{print $9}')
           body_bytes_sent=$(echo "$line" | awk '{print $10}')
           http_referer=$(echo "$line" | awk '{print $11}')
           http_user_agent=$(echo "$line" | awk '{for (i=12; i<NF; i++) printf $i" "; print $NF}')
           http_x_forwarded_for=$(echo "$line" | awk '{print $12}')
           server_name=$(echo "$line" | awk '{print $13}')
           http_host=$(echo "$line" | awk '{print $14}')
           http_connection=$(echo "$line" | awk '{print $15}')
           http_cache_control=$(echo "$line" | awk '{print $16}')
           http_upgrade_insecure_requests=$(echo "$line" | awk '{print $17}')
           http_accept=$(echo "$line" | awk '{print $18}')
           http_accept_encoding=$(echo "$line" | awk '{print $19}')
           http_accept_language=$(echo "$line" | awk '{print $20}')
           http_cookie=$(echo "$line" | awk '{print $21}')
           http_x_requested_with=$(echo "$line" | awk '{print $22}')
           http_dnt=$(echo "$line" | awk '{print $23}')
           http_if_modified_since=$(echo "$line" | awk '{print $24}')
           http_if_none_match=$(echo "$line" | awk '{print $25}')
           http_content_type=$(echo "$line" | awk '{print $26}')
           http_origin=$(echo "$line" | awk '{print $27}')
           http_content_length=$(echo "$line" | awk '{print $28}')
           http_via=$(echo "$line" | awk '{print $29}')
           http_x_forwarded_proto=$(echo "$line" | awk '{print $30}')
           http_x_forwarded_ssl=$(echo "$line" | awk '{print $31}')
           http_x_forwarded_port=$(echo "$line" | awk '{print $32}')
           http_x_real_ip=$(echo "$line" | awk '{print $33}')
           http_connection_upgrade=$(echo "$line" | awk '{print $34}')
           http_referer_encoding=$(echo "$line" | awk '{print $35}')


           # Insertar los campos en la base de datos MySQL
           mysql -h "$MYSQL_HOST" -u "$MYSQL_USER" -p"$MYSQL_PASS" "$MYSQL_DB" <<EOF
           INSERT INTO nginx_logs (time_local, remote_addr, remote_user, request, status, body_bytes_sent, http_referer, http_user_agent, http_x_forwarded_for, server_name, http_host, http_connection, http_cache_control, http_upgrade_insecure_requests, http_accept, http_accept_encoding, http_accept_language, http_cookie, http_x_requested_with, http_dnt, http_if_modified_since, http_if_none_match, http_content_type, http_origin, http_content_length, http_via, http_x_forwarded_proto, http_x_forwarded_ssl, http_x_forwarded_port, http_x_real_ip, http_connection_upgrade, http_referer_encoding)
           VALUES ('$time_local', '$remote_addr', '$remote_user', '$request', '$status', '$body_bytes_sent', '$http_referer', '$http_user_agent', '$http_x_forwarded_for', '$server_name', '$http_host', '$http_connection', '$http_cache_control', '$http_upgrade_insecure_requests', '$http_accept', '$http_accept_encoding', '$http_accept_language', '$http_cookie', '$http_x_requested_with', '$http_dnt', '$http_if_modified_since', '$http_if_none_match', '$http_content_type', '$http_origin', '$http_content_length', '$http_via', '$http_x_forwarded_proto', '$http_x_forwarded_ssl', '$http_x_forwarded_port', '$http_x_real_ip', '$http_connection_upgrade', '$http_referer_encoding');
EOF
       done
   fi
   # Guardar la última línea procesada
   tail -n 1 "$LOG_FILE" > "$LAST_LINE_FILE"
}


# Llamar a la función para procesar los logs
process_logs
